package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Retrieving all users
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved.";
	}

	// Creating a new user
	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}

	// Retrieving a specific user
	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " + userid + ".";
	}

	// Deleting a user
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@RequestHeader(value = "Authorization", required = false) String header, @PathVariable Long userid) throws Exception {
		if(header != null && !header.isEmpty()) {
			return "The user " + userid + " has been deleted.";
		}
		else{
			return ("Unauthorized access.");
		}
	}

	//Updating a user
	@PutMapping("/users/{userid}")
	//Automatically converts the format to JSON.
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}

}
